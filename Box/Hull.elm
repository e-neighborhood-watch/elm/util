module Util.Box.Hull
  exposing
    ( hull
    )


import Util.Box.Type
  exposing
    ( Box
    )
import Util.NonEmpty.Map
  as NonEmpty
import Util.NonEmpty.Maximum
  as NonEmpty
import Util.NonEmpty.Minimum
  as NonEmpty
import Util.NonEmpty.Type
  exposing
    ( NonEmpty
    )


-- | Gives the smallest box containing all the boxes in a list
hull : NonEmpty (Box number) -> Box number
hull boxes =
  { min =
    { x =
      NonEmpty.map (.x << .min) boxes
      |> NonEmpty.minimum
    , y =
      NonEmpty.map (.y << .min) boxes
      |> NonEmpty.minimum
    }
  , max =
    { x =
      NonEmpty.map (.x << .max) boxes
      |> NonEmpty.maximum
    , y =
      NonEmpty.map (.y << .max) boxes
      |> NonEmpty.maximum
    }
  }
