module Util.Box.Area
  exposing
    ( area
    )


import Util.Box.Type
  exposing
    ( Box
    )


area : Box number -> number
area { min, max } =
  (max.x - min.x) * (max.y - min.y)
