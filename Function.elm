module Util.Function
  exposing
    ( flip
    , uncurry
    )


flip : (a -> b -> c) -> (b -> a -> c)
flip f x y =
  f y x


uncurry : (a -> b -> c) -> ((a, b) -> c)
uncurry f (x, y) =
  f x y
