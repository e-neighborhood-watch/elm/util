module Util.NonEmpty.Type
  exposing
    ( NonEmpty
    )


type alias NonEmpty a =
  { head :
    a
  , tail :
    List a
  }
