module Util.NonEmpty.Concat
  exposing
    ( concat
    )


import Util.NonEmpty.Type
  exposing
    ( NonEmpty
    )


concat : NonEmpty a -> NonEmpty a -> NonEmpty a
concat a b =
  { head =
    a.head
  , tail =
    a.tail ++ b.head :: b.tail
  }
