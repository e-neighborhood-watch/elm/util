module Util.NonEmpty.Maximum
  exposing
    ( maximum
    )


import Util.NonEmpty.Type
  exposing
    ( NonEmpty
    )


maximum : NonEmpty comparable -> comparable
maximum {head, tail} =
  case
    List.maximum tail
  of
    Nothing ->
      head
    Just m ->
      max head m
