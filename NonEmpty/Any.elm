module Util.NonEmpty.Any
  exposing
    ( any
    )


import Util.NonEmpty.Type
  exposing
    ( NonEmpty
    )


any : (a -> Bool) -> NonEmpty a -> Bool
any pred {head, tail} =
  pred head || List.any pred tail
