module Util.NonEmpty.ConcatMap
  exposing
    ( concatMap
    )


import Util.NonEmpty.Type
  exposing
    ( NonEmpty
    )


concatMap : (a -> List b) -> NonEmpty a -> List b
concatMap f {head, tail} =
  f head ++ List.concatMap f tail
