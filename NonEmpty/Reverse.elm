module Util.NonEmpty.Reverse
  exposing
    ( reverse
    )


import Util.NonEmpty.Cons
  as NonEmpty
import Util.NonEmpty.Type
  exposing
    ( NonEmpty
    )


reverse : NonEmpty a -> NonEmpty a
reverse { head, tail } =
  List.foldl
    NonEmpty.cons
    { head =
      head
    , tail =
      []
    }
    tail
