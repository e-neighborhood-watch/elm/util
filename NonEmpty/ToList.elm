module Util.NonEmpty.ToList
  exposing
    ( toList
    )


import Util.NonEmpty.Type
  exposing
    ( NonEmpty
    )


toList : NonEmpty a -> List a
toList {head, tail} =
  head :: tail
