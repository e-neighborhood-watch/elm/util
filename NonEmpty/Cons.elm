module Util.NonEmpty.Cons
  exposing
    ( cons
    )


import Util.NonEmpty.Type
  exposing
    ( NonEmpty
    )

cons : a -> NonEmpty a -> NonEmpty a
cons x { head, tail } =
  { head =
    x
  , tail =
    head :: tail
  }
