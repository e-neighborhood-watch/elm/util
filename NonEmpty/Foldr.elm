module Util.NonEmpty.Foldr
  exposing
    ( foldr
    )


import Util.NonEmpty.Type
  exposing
    ( NonEmpty
    )


foldr : (a -> b -> b) -> b -> NonEmpty a -> b
foldr f start { head, tail } =
  List.foldr f start (head :: tail)
