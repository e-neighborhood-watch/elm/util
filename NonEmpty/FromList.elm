module Util.NonEmpty.FromList
  exposing
    ( fromList
    )


import Util.NonEmpty.Type
  exposing
    ( NonEmpty
    )


fromList : List a -> Maybe (NonEmpty a)
fromList list =
  case
    list
  of
    [] ->
      Nothing

    head :: tail ->
      Just
        { head =
          head
        , tail =
          tail
        }
