module Util.NonEmpty.Minimum
  exposing
    ( minimum
    )


import Util.NonEmpty.Type
  exposing
    ( NonEmpty
    )


minimum : NonEmpty comparable -> comparable
minimum {head, tail} =
  case
    List.minimum tail
  of
    Nothing ->
      head
    Just m ->
      min head m
