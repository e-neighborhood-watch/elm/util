module Util.NonEmpty.Map
  exposing
    ( map
    )


import Util.NonEmpty.Type
  exposing
    ( NonEmpty
    )


map : (a -> b) -> NonEmpty a -> NonEmpty b
map f xs =
  { head =
    f xs.head
  , tail =
    List.map f xs.tail
  }
