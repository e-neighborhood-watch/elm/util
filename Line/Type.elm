module Util.Line.Type
  exposing
    ( Line
    )


import Util.Position.Type
  exposing
    ( Position
    )


type alias Line a =
  { start :
    Position a
  , end :
    Position a
  }
