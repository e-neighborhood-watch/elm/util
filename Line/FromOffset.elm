module Util.Line.FromOffset
  exposing
    ( fromOffset
    )


import Util.Line.Type
  exposing
    ( Line
    )
import Util.Offset.Type
  exposing
    ( Offset
    )
import Util.Position.Translate
  as Position
import Util.Position.Type
  exposing
    ( Position
    )


fromOffset : Position number -> Offset number -> Line number
fromOffset pos off =
  { start =
    pos
  , end =
    Position.translate off pos
  }
