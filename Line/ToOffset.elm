module Util.Line.ToOffset
  exposing
    ( toOffset
    )


import Util.Line.Type
  exposing
    ( Line
    )
import Util.Offset.Type
  exposing
    ( Offset
    )
import Util.Position.Difference
  as Position


toOffset : Line number -> Offset number
toOffset l =
  Position.difference l.end l.start
