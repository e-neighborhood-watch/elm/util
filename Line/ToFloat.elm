module Util.Line.ToFloat
  exposing
    ( toFloat
    )


import Util.Line.Type
  exposing
    ( Line
    )
import Util.Position.ToFloat
  as Position


toFloat : Line Int -> Line Float
toFloat l =
  { start =
    Position.toFloat l.start
  , end =
    Position.toFloat l.end
  }
