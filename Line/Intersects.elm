module Util.Line.Intersects
  exposing
    ( intersects
    )


import Util.Line.ToOffset
  as Line
import Util.Line.Type
  exposing
    ( Line
    )
import Util.Offset.Slope
  as Offset


sep : Line Float -> Line Float -> Bool
sep l1 l2 =
  case
    l1 |> Line.toOffset |> Offset.slope
  of
    Nothing ->
        case
          ( compare l2.start.x l1.start.x
          , compare l2.end.x l1.end.x
          )
        of
          (LT, GT) ->
            True
          (GT, LT) ->
            True
          (EQ, EQ) ->
            not
              ( compare (l2.start.y) (l1.start.y) == compare (l2.end.y) (l1.start.y)
              || compare (l2.start.y) (l1.end.y) == compare (l2.end.y) (l1.end.y)
              )
          _ ->
            False
    Just slope1 ->
      let
        yIntercept1 : Float
        yIntercept1 =
          l1.start.y - (l1.start.x * slope1)
        f1 : Float -> Float
        f1 x =
          slope1 * x + yIntercept1
      in
        case
          ( compare (f1 l2.start.x) l2.start.y
          , compare (f1 l2.end.x) l2.end.y
          )
        of
          (LT, GT) ->
            True
          (GT, LT) ->
            True
          (EQ, EQ) ->
            not
              ( compare (l2.start.x) (l1.start.x) == compare (l2.end.x) (l1.start.x)
              || compare (l2.start.x) (l1.end.x) == compare (l2.end.x) (l1.end.x)
              )
          _ ->
            False


intersects : Line Float -> Line Float -> Bool
intersects l1 l2 =
  sep l1 l2 && sep l2 l1
