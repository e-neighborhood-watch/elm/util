module Util.Rotation.TurnClockwise
  exposing
    ( turnClockwise
    )


import Util.Rotation.Type
  exposing
    ( Rotation (..)
    )


turnClockwise : Rotation -> Rotation
turnClockwise rot =
  case
    rot
  of
    None ->
      CW90

    CW90 ->
      CW180

    CW180 ->
      CCW90

    CCW90 ->
      None
