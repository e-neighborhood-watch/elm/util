module Util.Rotation.ToDegrees
  exposing
    ( toDegrees
    )


import Util.Rotation.Type
  exposing
    ( Rotation (..)
    )


toDegrees : Rotation -> Int
toDegrees rot =
  case
    rot
  of
    None ->
      0

    CW90 ->
      90

    CW180 ->
      180

    CCW90 ->
      -90
