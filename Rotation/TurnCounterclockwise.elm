module Util.Rotation.TurnCounterclockwise
  exposing
    ( turnCounterclockwise
    )


import Util.Rotation.Type
  exposing
    ( Rotation (..)
    )


turnCounterclockwise : Rotation -> Rotation
turnCounterclockwise rot =
  case
    rot
  of
    None ->
      CCW90

    CW90 ->
      None

    CW180 ->
      CW90

    CCW90 ->
      CW180
