module Util.Rotation.Add
  exposing
    ( add
    )


import Util.Rotation.TurnClockwise
  as Rotation
import Util.Rotation.TurnCounterclockwise
  as Rotation
import Util.Rotation.Type
  as Rotation
  exposing
    ( Rotation
    )


add : Rotation -> Rotation -> Rotation
add r1 r2 =
  case
    r1
  of
    Rotation.None ->
      r2
    Rotation.CW90 ->
      Rotation.turnClockwise r2
    Rotation.CCW90 ->
      Rotation.turnCounterclockwise r2
    Rotation.CW180 ->
      case
        r2
      of
        Rotation.None ->
          Rotation.CW180
        Rotation.CW90 ->
          Rotation.CCW90
        Rotation.CCW90 ->
          Rotation.CW90
        Rotation.CW180 ->
          Rotation.None
