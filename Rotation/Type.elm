module Util.Rotation.Type
  exposing
    ( Rotation (..)
    )


type Rotation
  = None
  | CW90
  | CW180
  | CCW90
