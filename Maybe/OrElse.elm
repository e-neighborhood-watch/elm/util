module Util.Maybe.OrElse
  exposing
    ( orElse
    )


orElse : Maybe a -> Maybe a -> Maybe a
orElse x y =
  case
    x
  of
    Just _ ->
      x
    Nothing ->
      y
