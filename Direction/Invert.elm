module Util.Direction.Invert
  exposing
    ( invert
    )


import Util.Direction.Type
  exposing
    ( Direction (..)
    )


invert : Direction -> Direction
invert dir =
  case
    dir
  of
    North ->
      South
    South ->
      North
    East ->
      West
    West ->
      East
