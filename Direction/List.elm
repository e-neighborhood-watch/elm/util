module Util.Direction.List
  exposing
    ( list
    )


import Util.Direction.Type
  exposing
    ( Direction (..)
    )


list : List Direction
list =
  [ North
  , South
  , East
  , West
  ]
