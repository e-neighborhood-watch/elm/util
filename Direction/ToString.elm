module Util.Direction.ToString
  exposing
    ( toString
    )


import Util.Direction.Type
  exposing
    ( Direction (..)
    )


toString : Direction -> String
toString dir =
  case
    dir
  of
    North ->
      "North"

    South ->
      "South"

    East ->
      "East"

    West ->
      "West"
