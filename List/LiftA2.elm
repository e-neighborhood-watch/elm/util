module Util.List.LiftA2
  exposing
    ( liftA2
    )


liftA2 : (a -> b -> c) -> List a -> List b -> List c
liftA2 f m1 m2 =
  List.concatMap
    ( \ x1 ->
      List.map (f x1) m2
    )
    m1
