module Util.Map.Remove
  exposing
    ( remove
    )


import Dict


import Util.Map.Type
  exposing
    ( Map
    )
import Util.Position.ToPair
  as Position
import Util.Position.Type
  exposing
    ( Position
    )


remove : Position comparable -> Map comparable v -> Map comparable v
remove =
  Position.toPair >> Dict.remove
