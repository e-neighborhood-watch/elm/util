module Util.Map.Get
  exposing
    ( get
    , getFrom
    )


import Dict


import Util.Map.Type
  exposing
    ( Map
    )
import Util.Position.ToPair
  as Position
import Util.Position.Type
  exposing
    ( Position
    )


get : Position comparable -> Map comparable v -> Maybe v
get =
  Position.toPair >> Dict.get


getFrom : Map comparable v -> Position comparable -> Maybe v
getFrom m k =
  get k m
