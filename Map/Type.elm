module Util.Map.Type
  exposing
    ( Map
    )


import Dict
  exposing
    ( Dict
    )

type alias Map a b =
  Dict (a, a) b
