module Util.Map.Values
  exposing
    ( values
    )


import Dict


import Util.Map.Type
  exposing
    ( Map
    )


values : Map k v -> List v
values =
  Dict.values
