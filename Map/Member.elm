module Util.Map.Member
  exposing
    ( member
    , memberOf
    )


import Dict


import Util.Map.Type
  exposing
    ( Map
    )
import Util.Position.ToPair
  as Position
import Util.Position.Type
  exposing
    ( Position
    )


member : Position comparable -> Map comparable v -> Bool
member =
  Position.toPair >> Dict.member


memberOf : Map comparable v -> Position comparable -> Bool
memberOf m k =
  member k m
