module Util.Map.ToList
  exposing
    ( toList
    )


import Dict


import Util.Map.Type
  exposing
    ( Map
    )
import Util.Position.FromPair
  as Position
import Util.Position.Type
  exposing
    ( Position
    )


toList : Map k v -> List (Position k, v)
toList =
  Dict.toList >> List.map (Tuple.mapFirst Position.fromPair)
