module Util.Map.Empty
  exposing
    ( empty
    )


import Dict


import Util.Map.Type
  exposing
    ( Map
    )


empty : Map k v
empty =
  Dict.empty
