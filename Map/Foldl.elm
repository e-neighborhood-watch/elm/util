module Util.Map.Foldl
  exposing
    ( foldl
    )


import Dict


import Util.Map.Type
  exposing
    ( Map
    )
import Util.Position.FromPair
  as Position
import Util.Position.Type
  exposing
    ( Position
    )


foldl : (Position k -> v -> b -> b) -> b -> Map k v -> b
foldl func =
  Dict.foldl (Position.fromPair >> func)
