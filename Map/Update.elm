module Util.Map.Update
  exposing
    ( update
    )


import Dict


import Util.Map.Type
  exposing
    ( Map
    )
import Util.Position.ToPair
  as Position
import Util.Position.Type
  exposing
    ( Position
    )


update : Position comparable -> (Maybe v -> Maybe v) -> Map comparable v -> Map comparable v
update =
  Position.toPair >> Dict.update
