module Util.Map.Map
  exposing
    ( map
    , keyMap
    )


import Dict


import Util.Map.Type
  exposing
    ( Map
    )
import Util.Position.FromPair
  as Position
import Util.Position.ToPair
  as Position
import Util.Position.Type
  exposing
    ( Position
    )


map : (Position k -> a -> b) -> Map k a -> Map k b
map fun =
  Dict.map (Position.fromPair >> fun)


keyMap : (Position k -> Position comparable) -> Map k a -> Map comparable a
keyMap fun m =
  List.map
    ( \(key, value) ->
        (key |> Position.fromPair |> fun |> Position.toPair, value)
    )
    (Dict.toList m)
  |> Dict.fromList
