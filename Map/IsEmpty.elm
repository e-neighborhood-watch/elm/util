module Util.Map.IsEmpty
  exposing
    ( isEmpty
    )


import Dict


import Util.Map.Type
  exposing
    ( Map
    )


isEmpty : Map k v -> Bool
isEmpty =
  Dict.isEmpty
