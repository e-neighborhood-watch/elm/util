module Util.Map.Insert
  exposing
    ( insert
    )


import Dict


import Util.Map.Type
  exposing
    ( Map
    )
import Util.Position.ToPair
  as Position
import Util.Position.Type
  exposing
    ( Position
    )


insert : Position comparable -> v -> Map comparable v -> Map comparable v
insert =
  Position.toPair >> Dict.insert
