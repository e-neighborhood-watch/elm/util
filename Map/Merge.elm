module Util.Map.Merge
  exposing
    ( merge
    )

import Dict

import Util.Map.Type
  exposing
    ( Map
    )
import Util.Position.FromPair
  as Position
import Util.Position.Type
  exposing
    ( Position
    )

merge :
    (Position comparable -> a -> result -> result)
    -> (Position comparable -> a -> b -> result -> result)
    -> (Position comparable -> b -> result -> result)
    -> Map comparable a
    -> Map comparable b
    -> result
    -> result
merge a ab b =
  Dict.merge (Position.fromPair >> a) (Position.fromPair >> ab) (Position.fromPair >> b)
