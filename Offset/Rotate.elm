module Util.Offset.Rotate
  exposing
    ( rotate
    )


import Util.Offset.Type
  exposing
    ( Offset
    )
import Util.Rotation.Type
  exposing
    ( Rotation (..)
    )


rotateCW90 : Offset number -> Offset number
rotateCW90 { dx, dy } =
  { dx =
    negate dy
  , dy =
    dx
  }


rotate : Rotation -> Offset number -> Offset number
rotate rot =
  case
    rot
  of
    None ->
      identity

    CW90 ->
      rotateCW90

    CW180 ->
      rotateCW90
        >> rotateCW90

    CCW90 ->
      rotateCW90
        >> rotateCW90
        >> rotateCW90
