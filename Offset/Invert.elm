module Util.Offset.Invert
  exposing
    ( invert
    )

import Util.Offset.Type
  exposing
    ( Offset
    )

invert : Offset number -> Offset number
invert o =
  { dx =
    -o.dx
  , dy =
    -o.dy
  }

