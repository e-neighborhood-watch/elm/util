module Util.Offset.FromDirection
  exposing
    ( fromDirection
    )


import Util.Direction.Type
  as Direction
  exposing
    ( Direction
    )
import Util.Offset.Type
  exposing
    ( Offset
    )


fromDirection : Direction -> Offset Int
fromDirection dir =
  case
    dir
  of
    Direction.North ->
      { dx =
        0
      , dy =
        -1
      }

    Direction.South ->
      { dx =
        0
      , dy =
        1
      }

    Direction.East ->
      { dx =
        1
      , dy =
        0
      }

    Direction.West ->
      { dx =
        -1
      , dy =
        0
      }
