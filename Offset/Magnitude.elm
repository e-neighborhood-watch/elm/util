module Util.Offset.Magnitude
  exposing
    ( magnitude
    )


import Util.Offset.Type
  exposing
    ( Offset
    )


magnitude : Offset number -> number
magnitude { dx, dy } =
  dx^2 + dy^2
