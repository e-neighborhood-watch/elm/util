module Util.Offset.Add
  exposing
    ( add
    )


import Util.Offset.Type
  exposing
    ( Offset
    )


add : Offset number -> Offset number -> Offset number
add o1 o2 =
  { dx =
    o1.dx + o2.dx
  , dy =
    o1.dy + o2.dy
  }

