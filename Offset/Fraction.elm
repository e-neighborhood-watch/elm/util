module Util.Offset.Fraction
  exposing
    ( fraction
    )


import Util.Offset.Type
  exposing
    ( Offset
    )


fraction : Offset Float -> Offset Float
fraction { dx, dy } =
  { dx =
    dx - toFloat (floor dx)
  , dy =
    dy - toFloat (floor dy)
  }
