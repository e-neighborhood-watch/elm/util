module Util.Offset.Slope
  exposing
    ( slope
    )


import Util.Offset.Type
  exposing
    ( Offset
    )


slope : Offset Float -> Maybe Float
slope { dx, dy } =
  if
    dx == 0
  then
    Nothing
  else
    Just (dy / dx)
