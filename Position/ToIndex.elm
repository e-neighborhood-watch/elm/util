module Util.Position.ToIndex
  exposing
    ( toIndex
    )


import Util.Box.Type
  exposing
    ( Box
    )
import Util.Position.Type
  exposing
    ( Position
    )


toIndex : Box Int -> Position Int -> Int
toIndex levelBox position =
  ( position.y - levelBox.min.y ) * ( levelBox.max.x - levelBox.min.x ) + ( position.x - levelBox.min.x )

