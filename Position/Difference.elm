module Util.Position.Difference
  exposing
    ( difference
    )


import Util.Offset.Type
  exposing
    ( Offset
    )
import Util.Position.Type
  exposing
    ( Position
    )


difference : Position number -> Position number -> Offset number
difference p1 p2 =
  { dx =
    p2.x - p1.x
  , dy =
    p2.y - p1.y
  }
