module Util.Position.Move
  exposing
    ( move
    , moveBy
    )

import Util.Direction.Type
  exposing
    ( Direction
    )
import Util.Offset.FromDirection
  as Offset
import Util.Position.Translate
  as Position
import Util.Position.Type
  exposing
    ( Position
    )


move : Direction -> Position Int -> Position Int
move =
  Offset.fromDirection >> Position.translate


moveBy : Position Int -> Direction -> Position Int
moveBy x y =
  move y x
