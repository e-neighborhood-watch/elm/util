module Util.Position.ToFloat
  exposing
    ( toFloat
    )


import Util.Position.Type
  exposing
    ( Position
    )


toFloat : Position Int -> Position Float
toFloat pos =
  { x =
    Basics.toFloat pos.x
  , y =
    Basics.toFloat pos.y
  }
