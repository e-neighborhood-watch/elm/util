module Util.Position.Translate
  exposing
    ( translate
    )

import Util.Offset.Type
  exposing
    ( Offset
    )
import Util.Position.Type
  exposing
    ( Position
    )


translate : Offset number -> Position number -> Position number
translate { dx, dy } { x, y } =
  { x =
    x + dx
  , y =
    y + dy
  }
