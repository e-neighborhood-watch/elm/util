module Util.Either.Type
  exposing
    ( Either (..)
    )


type Either a b
  = Left a
  | Right b
